import React, { useState } from 'react'
import './App.css'
import dummyData from './data.json'
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
function App () {
  const [searchItem, setSearchItem] = useState('')

  const data = dummyData.filter((val) => {
    if (searchItem === '') {
      return val
    } else {
      return val.first_name.toLowerCase().includes(searchItem.toLowerCase())
    }
  })

  const tableData = data.map((value, key) => {
    return (
      <tr key={key}>
        <td>{value.id}</td>
        <td>{value.first_name}</td>
        <td>{value.last_name}</td>
        <td>{value.email}</td>
        <td>{value.gender}</td>
      </tr>
    )
  })
  return (
    <>
    <div className='title'>
      <h1>Search bar Task</h1>
    </div>
      <div className="search">
        <input
          type="text"
          placeholder="search....."
          onChange={(e) => {
            setSearchItem(e.target.value)
          }}
        />
      </div>
      <div className="container">
        <table className="table table-striped table-hover">
          <thead>
            <tr>
              <th>Id</th>
              <th>FirstName</th>
              <th>LastName</th>
              <th>Email</th>
              <th>Gender</th>
            </tr>
          </thead>
          <tbody>{tableData}</tbody>
        </table>
      </div>
    </>
  )
}

export default App
